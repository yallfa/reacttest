﻿import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Input, Button } from 'reactstrap';
import UtilityFunctions from '../../helpers/UtilityFunctionsHelper'
import Messages from '../../components/Messages';
import { Enumerations } from '../../helpers/EnumerationHelper';
import { ApiUrls } from '../../helpers/ApiUrlHelper';
import { AjaxGetRequest } from '../../helpers/HttpHelper';
import UserInfo from '../../helpers/UserInfoHelper';
import CircularProgress from '@material-ui/core/CircularProgress';

class FameHallDetail extends Component {

    //#region constructor

    constructor(props) {

        super(props);
        let id = this.props.match.params.id;
        this.state = {
            id: id,
            fameHallInfo: {},
            messages: [],
            messageUpdateDate: new Date(),
            redirectToPage: !id ? '/FameHalls' : '',
            isGetFameHallInProgress: false
        };

        this.getFameHallFromServer = this.getFameHallFromServer.bind(this);
        this.getFameHallFromServerCallback = this.getFameHallFromServerCallback.bind(this);
        if (!!id)
            this.getFameHallFromServer(id);
    }

    //#endregion


    //#region load data

    getFameHallFromServer(id) {
        this.setState({
            isGetFameHallInProgress: true
        })
        AjaxGetRequest(ApiUrls.FameDetail + '/' + id, null, this.getFameHallFromServerCallback);
    }

    getFameHallFromServerCallback(resultData) {

        let utilityFunctions = new UtilityFunctions();
        let errors = utilityFunctions.getReturnErroFromServer(resultData);
        console.info('***resultData = ', resultData);
        if (errors.length > 0) {

            this.setState({
                messages: errors,
                messageUpdateDate: new Date(),
                isGetFameHallInProgress: false
            });
        } else {

            
            let fameHallInfo = {
                id: resultData.data.id,
                name: resultData.data.name,
                dob: resultData.data.dob,
                image: resultData.data.image,
            }

            console.info('fameHallInfo', fameHallInfo);
            this.setState({
                fameHallInfo: fameHallInfo,
                isGetFameHallInProgress: false
            });
        }
    }

    //#endregion



    render() {
        return (

            <div >
                <Row>
                    <Col>
                        <h1>
                            Fame hall detail
                            {
                                this.state.isGetFameHallInProgress && <CircularProgress size={30} />
                            }
                        </h1>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Messages
                            messages={this.state.messages}
                            updateDate={this.state.messageUpdateDate}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col xs="2">
                        id
                    </Col>
                    <Col xs="4">
                        {this.state.fameHallInfo.id}
                    </Col>
                </Row>
                <Row>
                    <Col xs="2">
                        name
                    </Col>
                    <Col xs="4">
                        {this.state.fameHallInfo.name}
                    </Col>
                </Row>
                <Row>
                    <Col xs="2">
                        dob
                    </Col>
                    <Col xs="4">
                        {this.state.fameHallInfo.dob}
                    </Col>
                </Row>
                <Row>
                    <Col xs="2">
                        image
                    </Col>
                    <Col xs="4">
                        <img src={this.state.fameHallInfo.image} />
                    </Col>
                </Row>
            </div>
        );
    }
}

export default connect()(FameHallDetail);