﻿import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { actionCreators } from '../../store/FameHallItems';
import { Container, Row, Col, Input, Button, Table } from 'reactstrap';
import CircularProgress from '@material-ui/core/CircularProgress';

class FameHalls extends Component {


    //#region component lifecycle
    componentWillMount() {

        this.ensureDataFetched();
    }

    //#endregion

    //#region utilites

    ensureDataFetched() {
        const startDateIndex = parseInt(this.props.match.params.startDateIndex, 10) || 0;
        this.props.requestFameHallItems(startDateIndex);
    }

    //#endregion

    render() {

        if (!this.props.fameHallDataItems || this.props.fameHallDataItems.length == 0)
            return (
                <CircularProgress size={30} />
            );

        return (

            <div >
                <Row>
                    <Col>
                        <h1>
                            Halls of fame
                        </h1>
                    </Col>
                </Row>
                <Table striped>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Dob</th>
                            <th>Image</th>
                        </tr>
                    </thead>
                    {
                        this.props.fameHallDataItems.map((fameHallInfo, index) =>

                            <tr key={index}>
                                <td>
                                    <Link to={'/FameHallDetail/' + fameHallInfo.id}>
                                        {fameHallInfo.id}
                                    </Link>
                                </td>
                                <td>
                                    {fameHallInfo.name}
                                </td>
                                <td>
                                    {fameHallInfo.dob}
                                </td>
                                <td>
                                    <img src={fameHallInfo.image} />
                                </td>
                            </tr>
                        )
                    }
                </Table>
            </div>
        );
    }
}



//function renderPagination(props) {
//    const prevStartDateIndex = (props.startDateIndex || 0) - 5;
//    const nextStartDateIndex = (props.startDateIndex || 0) + 5;

//    return <p className='clearfix text-center'>
//        <Link className='btn btn-default pull-left' to={`/fetch-data/${prevStartDateIndex}`}>Previous</Link>
//        <Link className='btn btn-default pull-right' to={`/fetch-data/${nextStartDateIndex}`}>Next</Link>
//        {props.isLoading ? <span>Loading...</span> : []}
//    </p>;
//}

export default connect(
    state => state.fameHallItems,
    dispatch => bindActionCreators(actionCreators, dispatch)
)(FameHalls);
