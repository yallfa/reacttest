﻿import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { Container, Row, Col, Input, Button } from 'reactstrap';
import UtilityFunctions from '../../helpers/UtilityFunctionsHelper'
import Messages from '../../components/Messages';
import { Enumerations } from '../../helpers/EnumerationHelper';
import { ApiUrls } from '../../helpers/ApiUrlHelper';
import { AjaxPostRequest } from '../../helpers/HttpHelper';
import UserInfo from '../../helpers/UserInfoHelper';
import CircularProgress from '@material-ui/core/CircularProgress';
import { actionCreators } from '../../store/AuthenticationStatus';

class SignIn extends Component {

    //#region constructor

    constructor(props) {

        super(props);
        this.state = {
            userName: 'test',
            passWord: 'test',
            isLoginInProgress: false,
            messages: [],
            messageUpdateDate: new Date(),
            redirectToPage: ''
        };

        this.userNameChange = this.userNameChange.bind(this);
        this.passWordChange = this.passWordChange.bind(this);
        this.isFromValid = this.isFromValid.bind(this);
        this.siginContainerKeyPress = this.siginContainerKeyPress.bind(this);
        this.signInClick = this.signInClick.bind(this);
        this.sendSignInToServerCallback = this.sendSignInToServerCallback.bind(this);

    } 

    //#endregion

    //#region component lifecycle

    componentWillMount() {

        let userInfo = new UserInfo();
        if (userInfo.isAuthenticated())
            this.setState({ redirectToPage: '/FameHalls' });
    }

    //#endregion

    //#region change state

    userNameChange(e) {
        this.setState({ userName: e.target.value });
    }
    passWordChange(e) {
        this.setState({ passWord: e.target.value });
    }

    //#endregion

    //#region submit

    isFromValid() {

        let errorMessages = [];
        let isValid = true;
        let utilityFunctions = new UtilityFunctions();

        if (utilityFunctions.isEmpty(this.state.userName)) {

            errorMessages.push(utilityFunctions.getMessageItem('Fill user name!', Enumerations.MessageType.Error));
            isValid = false;
        }

        if (utilityFunctions.isEmpty(this.state.passWord)) {

            errorMessages.push(utilityFunctions.getMessageItem('Fill password!', Enumerations.MessageType.Error));
            isValid = false;
        }

        this.setState({
            messages: errorMessages,
            messageUpdateDate: new Date()
        });

        return isValid;
    }

    siginContainerKeyPress(e) {

        if (e.key == 'Enter') {
            this.signInClick();
        }
    }

    signInClick() {

        let isValid = this.isFromValid();
        if (!isValid)
            return;

        this.sendSignInToServer();
    }

    sendSignInToServer() {
        this.setState({
            isLoginInProgress: true
        })
        let parameters = {
            username: this.state.userName,
            password: this.state.passWord
        }
        AjaxPostRequest(ApiUrls.SignIn, parameters, this.sendSignInToServerCallback);
    }

    sendSignInToServerCallback(resultData) {

        let utilityFunctions = new UtilityFunctions();
        let errors = utilityFunctions.getReturnErroFromServer(resultData);
        if (errors.length > 0) {

            this.setState({
                messages: errors,
                messageUpdateDate: new Date(),
                isLoginInProgress: false
            });
        } else {

            this.props.setSignIn();
            this.setState({
                redirectToPage: "/FameHalls",
                isLoginInProgress: false
            });
        }
    }



    //#endregion

    render() {

        if (this.state.redirectToPage != '')
            return (<Redirect to={this.state.redirectToPage} />);

        return (
            <div onKeyPress={this.siginContainerKeyPress}>
                <Row>
                    <Col>
                        <h1>
                            Sign in
                        </h1>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Messages
                            messages={this.state.messages}
                            updateDate={this.state.messageUpdateDate}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col xs="2">
                        user name
                    </Col>
                    <Col xs="4">
                        <Input
                            type="text"
                            value={this.state.userName}
                            onChange={this.userNameChange}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col xs="2">
                        password
                    </Col>
                    <Col xs="4">
                        <Input
                            type="password"
                            value={this.state.passWord}
                            onChange={this.passWordChange}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col xs="2">
                        
                    </Col>
                    <Col xs="4">
                        <Button
                            color="primary"
                            disabled={this.state.isLoginInProgress}
                            onClick={this.signInClick}
                        >
                            sign in
                        </Button>
                        {
                            this.state.isLoginInProgress && <CircularProgress size={30} />
                        }
                    </Col>
                </Row>
            </div>
        );
    }
}

export default connect(
    state => state.isUserAuthenticate,
    dispatch => bindActionCreators(actionCreators, dispatch)
)(SignIn);