﻿import React, { Component, Suspense } from 'react';
import { Route, Redirect } from 'react-router-dom'
import Layout from './Layout';

const GuestLayoutRoute = ({ component: Component, ...rest }) => {

    return (
        <Route {...rest} render={matchProps => (
            <Layout>
                <Component {...matchProps} />
            </Layout>
        )} />
    )
};

export default GuestLayoutRoute;