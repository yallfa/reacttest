﻿import React, { Component, Suspense } from 'react';
import { Route, Redirect } from 'react-router-dom'
import Layout from './Layout';
import EnsureSignedIn from '../components/EnsureSignedIn';

const UserLayoutRoute = ({ component: Component, ...rest }) => {

    return (
        <Route {...rest} render={matchProps => (
            <Layout>
                <Component {...matchProps} />
                <EnsureSignedIn/>
            </Layout>
        )} />
    )
};

export default UserLayoutRoute;