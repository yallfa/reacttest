import React from 'react';
import { Collapse, Container, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import '../assets/css/NavMenu.css';
import UserInfo from '../helpers/UserInfoHelper'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../store/AuthenticationStatus';

class NavMenu extends React.Component {

    //#region constructor

    constructor(props) {
        super(props);

        let userInfo = new UserInfo();
        this.state = {
            isOpen: false,
            redirectToPage: '',
            isAuthenticat: userInfo.isAuthenticated()
        };

        this.toggle = this.toggle.bind(this);
        this.signOut = this.signOut.bind(this);
    }

    //#endregion

    //#region component lifecycle

    componentWillReceiveProps(newProps) {
        let userInfo = new UserInfo();
        this.setState({
            isAuthenticat: userInfo.isAuthenticated()
        });

    }

    //#endregion

    //#region menu functions

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    signOut() {

        this.props.setSignOut();
        //this.setState({
        //    isAuthenticat: false,
        //    redirectToPage: '/signin'
        //});
    }

    //#endregion

    render() {

        if (this.state.redirectToPage != '')
            return (<Redirect to={this.state.redirectToPage} />);

        return (
            <header>
                <Navbar className="navbar-expand-sm navbar-toggleable-sm border-bottom box-shadow mb-3" light >
                    <Container>
                        <NavbarBrand tag={Link} to="/">TestReact</NavbarBrand>
                        <NavbarToggler onClick={this.toggle} className="mr-2" />
                        <Collapse className="d-sm-inline-flex flex-sm-row-reverse" isOpen={this.state.isOpen} navbar>
                            <ul className="navbar-nav flex-grow">
                                <NavItem>
                                    <NavLink tag={Link} className="text-dark" to="/home">Home</NavLink>
                                </NavItem>
                                {
                                    !this.state.isAuthenticat &&
                                    <NavItem>
                                        <NavLink tag={Link} className="text-dark" to="/signin">Sing in</NavLink>
                                    </NavItem>
                                }
                                {
                                    this.state.isAuthenticat &&
                                    <NavItem>
                                        <NavLink tag={Link} className="text-dark" to="/famehalls">halls of fame</NavLink>
                                    </NavItem>
                                }
                                {
                                    this.state.isAuthenticat &&
                                    <NavItem>
                                        <NavLink
                                            tag={Link}
                                            className="text-dark"
                                            to="#"
                                            onClick={this.signOut}
                                        >
                                            Sing out
                                        </NavLink>
                                    </NavItem>
                                }
                            </ul>
                        </Collapse>
                    </Container>
                </Navbar>
            </header>
        );
    }
}

export default connect(
    state => state.isUserAuthenticate,
    dispatch => bindActionCreators(actionCreators, dispatch)
)(NavMenu);