﻿import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import { Container, Row, Col, Alert, Button } from 'reactstrap';
import { Enumerations } from '../helpers/EnumerationHelper';

import UserInfo from '../helpers/UserInfoHelper'
import { actionCreators } from '../store/AuthenticationStatus';

class EnsureSignedIn extends Component {

    //#region constructor

    constructor(props) {

        super(props);
        this.state = {
            redirectToPage: ''
        };
        this.checkAuthentication = this.checkAuthentication.bind(this);
    }

    //#endregion

    //#region component lifecycle

    checkAuthentication() {

        let userInfo = new UserInfo();
        if (!userInfo.isAuthenticated()) {

            this.setState({ redirectToPage: '/signin' });
        }
            
    }

    componentDidMount() {

        this.checkAuthentication();
    }

    componentWillReceiveProps() {
        this.checkAuthentication();
    }

    //#endregion

    render() {

        if (this.state.redirectToPage != '')
            return (<Redirect to={this.state.redirectToPage} />);

        return (
            <div>
            </div>
        );
    }
}

export default connect(
    state => state.isUserAuthenticate,
    dispatch => bindActionCreators(actionCreators, dispatch)
)(EnsureSignedIn);