﻿import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Alert, Button } from 'reactstrap';
import { Enumerations } from '../helpers/EnumerationHelper';

class Messages extends Component {

    //#region constructor

    constructor(props) {

        super(props);
        this.state = {
            messages: [],
            updateDate: props.updateDate
        };

    }

    //#endregion

    //#region props

    componentWillReceiveProps(newProps) {

        if (newProps.updateDate > this.state.updateDate) {

            this.setState({
                messages: newProps.messages,
                updateDate: newProps.updateDate
            });
        }

    }

    //#endregion

    //#region utility function

    getAlertColor(messageType) {

        var colorName = 'primary';

        switch (messageType) {
            case Enumerations.MessageType.Primary:
                colorName = "primary";
                break;
            case Enumerations.MessageType.Success:
                colorName = "success";
                break;
            case Enumerations.MessageType.Error:
                colorName = "danger";
                break;
        }

        return colorName;
    }

    //#endregion

    render() {

        return (
            <Row>
                <Col>
                    {
                        this.state.messages.map((messageInfo, index) =>
                            <Alert key={index}
                                color={this.getAlertColor(messageInfo.messageType)}>
                                {messageInfo.message}
                            </Alert>
                        )
                    }

                </Col>
            </Row>
        );
    }
}

export default Messages;