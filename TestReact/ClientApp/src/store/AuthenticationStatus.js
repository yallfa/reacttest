﻿import UserInfo from '../helpers/UserInfoHelper'
let userInfo = new UserInfo();
const signInType = 'SignIn';
const signOutType = 'SignOut';
const initialState = { isUserAuthenticate: userInfo.isAuthenticated() };

export const actionCreators = {
    setSignIn: () => ({ type: signInType }),
    setSignOut: () => ({ type: signOutType })
};

export const reducer = (state, action) => {
    state = state || initialState;

    console.info('***action = ', action);
    if (action.type === signInType) {
        userInfo.setSignIn();
        return { ...state, isUserAuthenticate: true };
    }

    if (action.type === signOutType) {
        userInfo.setSignOut();
        return { ...state, isUserAuthenticate: false };
    }

    return state;
};
