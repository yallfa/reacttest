﻿import { ApiUrls } from '../helpers/ApiUrlHelper'
import { AjaxPostRequest, AjaxGetRequest } from '../helpers/HttpHelper'

const requestFamehallsType = 'REQUEST_WEATHER_FameHalls';
const receivtFamehallsType = 'RECEIVE_WEATHER_FameHalls';
const initialState = { fameHallDataItems: [], isLoading: false };

export const actionCreators = {
    requestFameHallItems: startDateIndex => async (dispatch, getState) => {
        
        if (startDateIndex === getState().fameHallItems.startDateIndex) {
            // Don't issue a duplicate request (we already have or are loading the requested data)
            return;
        }

        dispatch({ type: requestFamehallsType, startDateIndex });
        AjaxGetRequest(ApiUrls.Fames, null, function (resultData) {

            if (resultData.data != null && resultData.data.list != null) {
                dispatch({ type: receivtFamehallsType, startDateIndex, fameHallDataItems: resultData.data.list });
            }
                
        });
        //const url = `api/SampleData/WeatherForecasts?startDateIndex=${startDateIndex}`;
        //const response = await fetch(url);
        //const forecasts = await response.json();
        //
        //dispatch({ type: receivtFamehallsType, startDateIndex, forecasts });
    }
};

export const reducer = (state, action) => {
    state = state || initialState;

    if (action.type === requestFamehallsType) {
        return {
            ...state,
            startDateIndex: action.startDateIndex,
            isLoading: true
        };
    }

    if (action.type === receivtFamehallsType) {
        return {
            ...state,
            startDateIndex: action.startDateIndex,
            fameHallDataItems: action.fameHallDataItems,
            isLoading: false
        };
    }

    return state;
};
