﻿const authentionStatue = 'AuthentionStatue';

export default class UserInfo {

    setSignIn() {
        window.localStorage.setItem(authentionStatue, true);
    }

    setSignOut() {
        window.localStorage.removeItem(authentionStatue);
        console.info('***window.localStorage.getItem(authentionStatue)  = ', window.localStorage.getItem(authentionStatue));
    }

    isAuthenticated() {

        if (window.localStorage.getItem(authentionStatue))
            return true;
        else
            return false;
    }
}