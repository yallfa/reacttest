﻿import { Enumerations } from "./EnumerationHelper";
import UserInfo from "./UserInfoHelper";

export default class UtilityFunctions {

    isEmpty(value) {

        if (value)
            return false;
        return true;
    }


    getMessageItem(message, messageType) {

        return {
            message: message,
            messageType: messageType
        }
    }

    getReturnErroFromServer(data) {

        let errors = [];
        if (data == null)
            errors.push(this.getMessageItem('No data!', Enumerations.MessageType.Error));
        else if (data.error != null) {
            if (data.error == 'not logged in') {
                let userInfo = new UserInfo();
                userInfo.setSignOut();
            }
            errors.push(this.getMessageItem(data.error, Enumerations.MessageType.Error));
        }
        else if (data.data.success === false)
            errors.push(this.getMessageItem('not success', Enumerations.MessageType.Error));

        return errors;
    }
}