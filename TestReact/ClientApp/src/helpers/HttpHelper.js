﻿import Cookies from 'universal-cookie';

function BaseUrl() {

    return 'https://halloffame-server.herokuapp.com/';
}

function GetHeader() {
    return {
        //'Authorization': 'Bearer ' + tokenInfo,
        'Content-Type': 'application/json',
        'Cookie': 'koa:sess=eyJhdXRoIjp0cnVlLCJfZXhwaXJlIjoxNTczODI2NjQ4MTI5LCJfbWF4QWdlIjo4NjQwMDAwMH0=;koa:sess.sig=BD7OQ7O4ibSp1LnpED5jVmqMEVA'
    };
}

function getVersion() {

    return 1;
}

export async function AjaxGetRequest(url, parameters, callBackFunction) {

    let strParameters = '?version=' + getVersion() +'&guest=true';
    if (parameters != null) {

        let keys = Object.keys(parameters);
        for (let index = 0; index < keys.length; index++) {
            strParameters += '&' + keys[index] + '=' + parameters[keys[index]];
        }
    }

    const cookies = new Cookies();

    url = BaseUrl() + url + strParameters;
    let headerInfo = GetHeader();

    let init = {
        method: 'GET', accept: 'application/json', mode: 'cors',
        crossdomain: true,
        //mode: 'cors',
        headers: new Headers(headerInfo)
        //cookie: 'koa:sess=eyJhdXRoIjp0cnVlLCJfZXhwaXJlIjoxNTczODI2NjQ4MTI5LCJfbWF4QWdlIjo4NjQwMDAwMH0=;koa:sess.sig=BD7OQ7O4ibSp1LnpED5jVmqMEVA'
    };

    //document.cookie = "koa:sess=eyJhdXRoIjp0cnVlLCJfZXhwaXJlIjoxNTczODI2NjQ4MTI5LCJfbWF4QWdlIjo4NjQwMDAwMH0=;koa:sess.sig=BD7OQ7O4ibSp1LnpED5jVmqMEVA";
    //
    //console.info('***init = ', init, url, document.cookie);
    const response = await fetch(url, init);
    const result = await response.json();

    callBackFunction(result);
}
export async function AjaxPostRequest(url, parameters, callBackFunction) {

    if (parameters == null) {
        parameters = {};
    }

    parameters.version = getVersion();

    url = BaseUrl() + url;
    let headerInfo = GetHeader();
    let strBody = JSON.stringify(parameters);
    let init = {
        method: 'Post',
        accept: 'application/json',
        mode: 'cors',
        crossdomain: true,
        body: strBody,
        headers: new Headers(headerInfo)
    };


    const response = await fetch(url, init);
    const result = await response.json();


    //console.info('*** response.headers = ', response, document.cookie);
    //console.info('*** response.headers.entries', response.getResponseHeader('Set-Cookie'));

    callBackFunction(result);
}