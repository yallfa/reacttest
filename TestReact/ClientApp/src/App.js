import React from 'react';
import { Route, Switch } from 'react-router';

import Home from './views/public/Home';
import SignIn from './views/authentication/SignIn';
import FameHalls from './views/fameHall/FameHalls';
import FameHallDetail from './views/fameHall/FameHallDetail';

import GuestLayoutRoute from "./containers/GuestLayoutRoute";
import UserLayoutRoute from "./containers/UserLayoutRoute";

export default () => (

    <Switch>
        <GuestLayoutRoute path='/Home' component={Home} />
        <GuestLayoutRoute exact path='/' component={SignIn} />
        <GuestLayoutRoute path='/SignIn' component={SignIn} />
        <UserLayoutRoute path='/FameHalls' component={FameHalls} />
        <UserLayoutRoute path='/FameHallDetail/:id?' component={FameHallDetail} />

    </Switch>
);
